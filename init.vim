" Make space leader character
"
let mapleader=" "

" Use system clipboard
set clipboard+=unnamedplus
"
" Vertically center the screen when entering Instert mode
" autocmd InsertEnter * norm zz

" Set theme
" Enable tab completion
set wildmode=longest,list,full
set nocompatible
set nobackup
set noswapfile

set termguicolors

" VimWiki dependencies
let g:vimwiki_list = [{'path': '~/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]
filetype plugin on
let g:instant_markdown_autostart = 0

" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype indent plugin on
 
" Enable syntax highlighting
syntax on

" Start NODEtree when vim opens
map <C-m> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

set hidden

" Better command-line completion
set wildmenu
 
" Show partial commands in the last line of the screen
" set showcmd

" Dont end lines
" set nowrap
set textwidth=0 wrapmargin=0 " https://stackoverflow.com/questions/2280030/how-to-stop-line-breaking-in-vim

" Highlight searches (use <C-L> to temporarily turn off highlighting; see the
" mapping of <C-L> below)
" set hlsearch
set incsearch

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase
 
set autoindent
 
set ruler
 
set confirm
 
" Use visual bell instead of beeping when doing something wrong
set visualbell

" Enable use of the mouse for all modes
set mouse=a

" Display column and row of cursor
set cursorline
set cursorcolumn

set number relativenumber

augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END
set notimeout ttimeout ttimeoutlen=200

" Tab options
set shiftwidth=4
set softtabstop=4
set expandtab
set tabstop=4
set smartindent  

" Put new vertical split to the right and new horizontal split at the botton
set splitbelow splitright

" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
map Y y$
nnoremap <Leader>o o<Esc>
nnoremap <Leader>O O<Esc> " Spacebar and O for new line above without entering insert mode
nnoremap <Leader>q :q<CR>
nnoremap <Leader>Q :q!<CR>
nnoremap <Leader>w :w<CR>
" Navigate through vim splits using spacebar and vim keybindings
nnoremap <Leader>h :wincmd h<CR>
nnoremap <Leader>l :wincmd l<CR>
nnoremap <Leader>j :wincmd j<CR>
nnoremap <Leader>k :wincmd k<CR>
nnoremap <Enter> <Nop>
" nnoremap <Enter> @="m`O\eg``"<cr>
map <Enter> i<Enter><ESC>
" nnoremap <BS> <Nop>
nnoremap <BS> i<BS><Right><ESC>
nnoremap <space><space> i<space><Right><ESC>
" Unbind arrow keys
noremap <Up>    <Nop>
noremap <Down>  <Nop>
noremap <Left>  <Nop>
noremap <Right> <Nop>

call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'vimwiki/vimwiki' " VimWiki for taking notes
Plug 'suan/vim-instant-markdown', {'for': 'markdown'} " View vimwiki markdown in browser
Plug 'preservim/nerdcommenter'
Plug 'gruvbox-community/gruvbox'
Plug 'rust-lang/rust.vim' " Plugin for rust programming
Plug 'ThePrimeagen/vim-be-good' " Practice VIM skills and speed
Plug 'mileszs/ack.vim'
Plug 'ycm-core/YouCompleteMe', { 'do': './install.py' }
call plug#end()

colorscheme gruvbox " Call plugin functions after Plug#end()
set background=dark

